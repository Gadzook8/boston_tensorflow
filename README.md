# Realistic ML with TensorFlow
In this notebook well use TensorFlow to make a verry simple <a href="https://en.wikipedia.org/wiki/Rectifier_(neural_networks)">ReLU</a> feed forward nerual network. Then well use [Stocastic gradiant descent](https://en.wikipedia.org/wiki/Stochastic_gradient_descent) to train model to map a set of features to a label. Next we can export our trained model to a [Protocol Buffer](https://developers.google.com/protocol-buffers/) so that its a bit more portable. Then well make a docker container that can server and manage our model using gRPC, and finaly we will run through a quick demo of how to access our production ready model.

## Directory Structure
```
├── BostonTensorflow.ipynb
├── client
│   ├── gRPCclient.ipynb
│   ├── model_pb2.py
│   ├── model_pb2.pyc
│   ├── prediction_service_pb2.py
│   ├── prediction_service_pb2.pyc
│   ├── predict_pb2.py
│   └── predict_pb2.pyc
├── data
│   ├── boston_predict.csv
│   ├── boston_test.csv
│   └── boston_train.csv
├── export
│   └── 00000001
│       ├── checkpoint
│       ├── export-00000-of-00001
│       └── export.meta
├── README.md
└── server
    └── Dockerfile

/BostonTensorflow.ipynb: Our main notebook, the place that our model is defined and trained

/client: A gRPC client designed access our model in a production friendly manner.

/data: Our data set in csv format

/export: A directory for our exported models, one is included in case you cant train a model on your hardware.

/server: A Dockerfile that will containerize our model so that it can be deployed and scaled easily.
```

## The data
Our data for this demonstration is the [boston housing data](https://archive.ics.uci.edu/ml/datasets/Housing) from the [UCI Machine Learning Repository](https://archive.ics.uci.edu/ml/index.html). The original data set includes 506 data points, and each data point has 14 attributes. Our dataset is a bit diffrent, its catagorical features have been striped ("yes, nerual networks can absolutly model regression with catagorical features, but it adds a significant amount of complexity, and is beyond the scope of this notebook"), and it has been broken up into three subsets, training, testing, and validation/prediction. The training set is the largest with 400 instances, it will be used to train the models. The testing is used to prevent overtraining ("memorization of dataset as opposed to a functional description of dataset"), it contains 100 instances, its periodicaly run through the models as theyer trained to evaluate theyer preformance. Finaly, the validation set is used to demo the final model against whatever is traditionaly used to predict labels, usually a human. In most cases our datas sets will be much larger and will require a significant amount of preprecessing using traditional methods, but again, no need to put ourselfs through that just yet.

<b>Attribute Information:</b></p>
<p class="normal">    1. CRIM:      per capita crime rate by town
<br>    2. ZN:        proportion of residential land zoned for lots over 25,000 sq.ft.
<br>    3. INDUS:     proportion of non-retail business acres per town
<br>    4. CHAS:      Charles River dummy variable (= 1 if tract bounds river; 0 otherwise)
<br>    5. NOX:       nitric oxides concentration (parts per 10 million)
<br>    6. RM:        average number of rooms per dwelling
<br>    7. AGE:       proportion of owner-occupied units built prior to 1940
<br>    8. DIS:       weighted distances to five Boston employment centres
<br>    9. RAD:       index of accessibility to radial highways
<br>    10. TAX:      full-value property-tax rate per $10,000
<br>    11. PTRATIO:  pupil-teacher ratio by town
<br>    12. B:        1000(Bk - 0.63)^2 where Bk is the proportion of blacks by town
<br>    13. LSTAT:    % lower status of the population
<br>    14. MEDV:     Median value of owner-occupied homes in $1000's</p>

## TensorFlow
TensorFlow is a ML library that google opensourced about a year ago, since then it has gained alot of popularity, and today its probably the best option for somebody looking to get into neural network based ML. TensorFlow is writin primarly in C++ and thats where the majority of the work happens. On a low level, TensorFlow allows you to represent things as <a href="https://en.wikipedia.org/wiki/Directed_graph">Di-graphs</a>, then it does lots of algebra relitivly efficiently to solve for variables within the graph. Its up to you to make that into something meaningfull. TensorFlow has a verry complete python API and a sortof no nonsense C++ api, it supports googles protobuffer and it can run on a variety of hardware configurations ranging from your desktop, to the cloud, to application specific cluster with hadoop and CUDA or openCL, it can evean run on embedded devices ("when you say ok google, thats accatulay a Tensorflow model your talking to"). I dont belive TensorFlow is the end all be all of machine learning, it represents a verry small and somewhat conservative idea of what machine learning can be. That said, TensorFlow does what it does well, and is one of the first really production oriented ML projects.

## Google
Google has a tensdency to do things a bit diffrent from most everybody else. TensorFlow is no exception, its fairly easy to get into on its own, but if you want to use it in prodction your going to have to use a few of googles other tools. In addition to TensorFlow this notebook uses [Protocol Buffers](https://developers.google.com/protocol-buffers/), [Bazel](https://bazel.build/), and [gRPC](http://www.grpc.io/). You may want to take a moment to just look over what these are real quick. If not, protobuff is basicly googles anwer to XML, Bazel is a build tool that was designed to work with remote build clusters, and gRPC is what google uses for comunication between microservices and clients. You dont have to go too indepth on this unless your planning on really building TensorFlow infastructure and you need the ability to do things like create self training, autoscaling, contiuouse integrationish solutions with TensorFlow. As you see these tools in the notebook just know what they are but dont worry to much about why or how they work.

## BostonTensorflow notebook
The rest of this notebook is pretty much documented in place, once youve looked at the data you can open up the BostonTensorflow notebook and run through it.
