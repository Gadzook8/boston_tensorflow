# Deploying our TensorFlow model
There are a number of ways that we couold deploy our model, but for this tutorial well use docker and [tensorflows serving library](https://tensorflow.github.io/serving/architecture_overview). The serving library allows users to design servers that can handle things like continuouse integration, A/B testing, distruted models, etc. In our case we our Dockrerfile will just grab an export directory and make a container that serves those models. Then that you can deploy that container with docker swarm or kubernetes and scaling and lifecycle managment should be pretty hastle free. Our server exposes our model using [gRPC](http://www.grpc.io/), gRPC is what googles uses internaly, that said, it can easily be converted into a RESTful api if thats your thing. In this case gRPC allows us to provide the people that use our model with a verry consistant simple means of access. Suppose we deploy our trained model and somebody wirtes a program that uses it. Then in 3 months we decide we want to train a new more accurate model with more recent data, with this system we can easily train our new model, and deploy it without our users having to change a single line of code. You can build this continer with:
```
cp -avr ../export .
```
Then you can build with:
```
docker build -t $USER/boston_tf .
```
Then you can run your contianer with:
```
docker run -P -d $USER/boston_tf --model_name=boston
```
Then you can check what port your server is running on with:
```
docker ps
```
